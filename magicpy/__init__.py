from .magicpy import MagVenture
from .utils import *
from .stimulator import *

__version__ = "0.1.5"
