import os
import sys

sys.path.insert(0, os.path.abspath('../..'))
extensions = [
'sphinx.ext.autodoc',]

project = 'MAGICpy'
copyright = '2021, Ole Numssen'
author = 'Ole Numssen'
version = ''
release = '0.1'
templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
pygments_style = 'sphinx'
html_theme = 'alabaster'
html_static_path = ['_static']